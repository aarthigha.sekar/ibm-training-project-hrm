package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import org.openqa.selenium.By;


public class HRM4 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();

		Thread.sleep(3000);

		//PIM
		WebElement PIM = driver.findElement(By.xpath("//a[@id='menu_pim_viewPimModule']"));
		PIM.click();
		Thread.sleep(3000);

		//Add Employee
		WebElement Addemp = driver.findElement(By.xpath("//a[@id='menu_pim_addEmployee']"));
		Addemp.click();

		WebElement FName = driver.findElement(By.xpath("//input[@id='firstName']"));
		FName.sendKeys("Firaar4");

		WebElement LName = driver.findElement(By.xpath("//input[@id='lastName']"));
		LName.sendKeys("Lasaar2");

		WebElement Save = driver.findElement(By.xpath("//input[@value='Save']"));
		Save.click();

		WebElement emplist = driver.findElement(By.xpath("//a[@id='menu_pim_viewEmployeeList']"));
		emplist.click();

		//Look for added employee in employee list
		List<WebElement> findElements = driver.findElements(By.xpath("//table[@id='resultTable']//td[3]/a"));
		int size = findElements.size();
		System.out.println("size"+size);
		
			for (WebElement webElement : findElements) {
				String text = webElement.getText();
				if(text.equalsIgnoreCase("Firaar4")) {
					
					System.out.println("emp added"+text);
					driver.close();	
				}
			/*
			 * else { System.out.println("emp not added"); }
			 */
			}
		

	}


}


