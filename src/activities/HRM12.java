package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import org.openqa.selenium.WebElement;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;


public class HRM12 {

	public static void main(String[] args) throws InterruptedException {
		
				
			System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("http://alchemy.hguy.co/orangehrm");
			driver.manage().window().maximize();
			Thread.sleep(3000);
			
			//Login into webpage
			WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
			Username.sendKeys("orange");

			WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
			password.sendKeys("orangepassword123");

			WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
			Login.click();
			
			Thread.sleep(3000);
			
							 
			
			//PIM
			WebElement PIM = driver.findElement(By.xpath("//a[@id='menu_pim_viewPimModule']"));
			PIM.click();
			
			WebElement emplist = driver.findElement(By.xpath("//a[@id='menu_pim_viewEmployeeList']"));
			emplist.click();
						 
			WebElement empadd = driver.findElement(By.xpath("//input[@name='btnAdd']"));			
			Actions a=new Actions(driver);
			a.moveToElement(empadd).click().build().perform();
			empadd.click();
						
			WebElement loginCB = driver.findElement(By.xpath("//input[@id='chkLogin']"));	
			loginCB.click();
			//Actions a=new Actions(driver);
			//a.tick().click().build().perform();
			
									
	        //Load CSV file
	        
			CSVReader reader = null;
			try {
				reader = new CSVReader(new FileReader("C:\\Users\\AarthighaThoppe\\Desktop\\empdetailsaarhrm1.csv"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String[] csvCell;
			
			try {
				while ((csvCell = reader.readNext()) != null)
				{		
				    String Fname = csvCell[0];
					String Lname = csvCell[1];
					String Uname = csvCell[2];
					String Pwd1 = csvCell[3];
					String pwd2 = csvCell[4];
					String status = csvCell[5];
					
					     	    
				WebElement FName = driver.findElement(By.xpath("//input[@id='firstName']"));
				FName.sendKeys(Fname);
				
				WebElement LName = driver.findElement(By.xpath("//input[@id='lastName']"));
				LName.sendKeys(Lname);

				WebElement UName = driver.findElement(By.xpath("//input[@id='user_name']"));
				UName.sendKeys(Uname);
				
				WebElement Status = driver.findElement(By.xpath("//Select[@id='status']"));
				Select s = new Select(Status);
				s.selectByValue(status);
				
				System.out.println("Emp list saved");	
				Thread.sleep(4000);
				

				/*
							
				WebElement value = driver.findElement(By.xpath("(//a[contains(text(),'aarhrmfirst')])[1]"));
				boolean displayed = value.isDisplayed();
				if(displayed==true) {
				System.out.println("Emp list verified");
				}
				else
				{
					System.out.println("emp list not added");
				}    */
				
				WebElement Save = driver.findElement(By.xpath("//input[@id='btnSave']"));			
				Save.click();
				
				driver.switchTo().alert().accept();
				
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	 
				driver.close();
}
			} catch (CsvValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		    

	}
}

