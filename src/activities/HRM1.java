package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HRM1 {

	public static void main(String[] args) throws InterruptedException {
		

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Open Browser
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		String title = driver.getTitle();
		
		//Print Title
		System.out.println("title is " + title);
		
		//Match Title
		if(title.equalsIgnoreCase("OrangeHRM")) {
			System.out.println("title matches as " + title);
			driver.close();
		}
		else
		{
			System.out.println("title not matched as expected");
		}

	}

}
