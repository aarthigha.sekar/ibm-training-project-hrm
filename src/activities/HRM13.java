package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;

import java.awt.Desktop.Action;
import java.util.List;

import org.openqa.selenium.By;


public class HRM13 {

	public static void main(String[] args) throws InterruptedException {
		
		
			System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("http://alchemy.hguy.co/orangehrm");
			driver.manage().window().maximize();
			Thread.sleep(3000);
			
			//Login into webpage
			WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
			Username.sendKeys("orange");

			WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
			password.sendKeys("orangepassword123");

			WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
			Login.click();
			
			Thread.sleep(3000);
			
			//PIM
			WebElement PIM = driver.findElement(By.xpath("//a[@id='menu_pim_viewPimModule']"));
			PIM.click();
						 
			WebElement configuration = driver.findElement(By.xpath("//a[@id='menu_pim_Configuration']"));			
			Actions a = new Actions(driver);
			a.moveToElement(configuration).click().build().perform();
						
			WebElement dataimp = driver.findElement(By.xpath("//li//a[@id='menu_admin_pimCsvImport']"));			
			a.moveToElement(dataimp).click().build().perform();
			dataimp.click();
				    
			WebElement fileselect = driver.findElement(By.xpath("//input[@id='pimCsvImport_csvFile']"));			
			a.moveToElement(fileselect).click().build().perform();			
			fileselect.sendKeys("C:\\Users\\AarthighaThoppe\\Desktop\\empdetailsaarhrm.csv");
				     	    
			WebElement upload = driver.findElement(By.xpath("//input[@id='btnSave']"));
			upload.click();

			System.out.println("Emp list saved");	
			Thread.sleep(4000);
			
			WebElement Emplist = driver.findElement(By.xpath("//a[@id='menu_pim_viewEmployeeList']"));
			Emplist.click();
			
						
			WebElement value = driver.findElement(By.xpath("(//a[contains(text(),'aarhrmfirst')])[1]"));
			boolean displayed = value.isDisplayed();
			if(displayed==true) {
			System.out.println("Emp list verified");
			}
			else
			{
				System.out.println("emp list not added");
			}    
			
				 
			driver.close();
		} 
		    


}

