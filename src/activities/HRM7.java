package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;


public class HRM7 {

	
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();
		
		Thread.sleep(3000);
		
		//Myinfo 
		WebElement myinfo = driver.findElement(By.xpath("//a[@id='menu_pim_viewMyDetails']//b"));
		myinfo.click();
		
		//qualification option
		WebElement qualification = driver.findElement(By.xpath("(//a[text()='Qualifications'])[2]"));
		qualification.click();
		
		//addexp 
		Thread.sleep(2000);
		WebElement addexp = driver.findElement(By.xpath("//input[@id='addWorkExperience']"));
		addexp.click();
		
		WebElement company=driver.findElement(By.xpath("//input[@id='experience_employer']"));
		company.sendKeys("IBMAAR");
		
		WebElement jobtitle=driver.findElement(By.xpath("//input[@id='experience_jobtitle']"));
		jobtitle.sendKeys("Test Lead aar");
		
		 WebElement fromdate = driver.findElement(By.xpath("//input[@id='experience_from_date']"));
		    fromdate.click();
		    fromdate.clear();
		    WebElement datepick = driver.findElement(By.xpath("//a[text()='1']"));
		    datepick.click();
		    
		/*
		 * fromdate.sendKeys("2010-02-13"); fromdate.equals(fromdate); fromdate.click();
		 */
		    
        WebElement todate = driver.findElement(By.xpath("//input[@id='experience_to_date']"));
		    todate.click();
		    todate.clear();
		    WebElement datetopick = driver.findElement(By.xpath("//a[text()='3']"));
		    datetopick.click();
		   
		WebElement save = driver.findElement(By.xpath("//input[@id='btnWorkExpSave']"));
			save.click();
				
	    System.out.println("Qualification added");
	    
		driver.close();
		
	   
		
		
	}


}


