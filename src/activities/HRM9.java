package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import org.openqa.selenium.By;


public class HRM9 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();

		Thread.sleep(3000);

		//Myinfo 
		WebElement myinfo = driver.findElement(By.xpath("//a[@id='menu_pim_viewMyDetails']//b"));
		myinfo.click();

		//Emergency contact
		WebElement Emgycontact = driver.findElement(By.xpath("//a[text()='Emergency Contacts']"));
		Emgycontact.click();

		List<WebElement> contactlist = driver.findElements(By.xpath("//table[@id='emgcontact_list']//td[2]"));
		int size = contactlist.size();
		System.out.println("size"+size);
		for(int i =0 ; i <= size ; i++) {
			List<WebElement> name = driver.findElements(By.xpath("(//table[@id='emgcontact_list']//td[2])["+i+"]"));
			for (WebElement webElement : name) {
				String text = webElement.getText();
				System.out.println("  ");
				System.out.println("name is " + text);
			}
			List<WebElement> relation = driver.findElements(By.xpath("(//table[@id='emgcontact_list']//td[3])["+i+"]"));
			for (WebElement webElements : relation) {
				String text = webElements.getText();
				System.out.println("relations is " + text);
			}
			List<WebElement> tele = driver.findElements(By.xpath("(//table[@id='emgcontact_list']//td[4])["+i+"]"));
			for (WebElement webElements : tele) {
				String text = webElements.getText();
				System.out.println("telephone is " + text);
			}
			List<WebElement> mob = driver.findElements(By.xpath("(//table[@id='emgcontact_list']//td[5])["+i+"]"));
			for (WebElement webElements : mob) {
				String text = webElements.getText();
				System.out.println("mobile is " + text);
			}
			List<WebElement> workphne = driver.findElements(By.xpath("(//table[@id='emgcontact_list']//td[6])["+i+"]"));
			for (WebElement webElements : workphne) {
				String text = webElements.getText();
				System.out.println("workphone is " + text);
			}

		}
			

		driver.close();




	}


}


