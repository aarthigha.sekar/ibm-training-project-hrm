package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;


public class HRM5 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();
		
		Thread.sleep(3000);
		
		//PIM		
		WebElement myinfo = driver.findElement(By.xpath("//a[@id='menu_pim_viewMyDetails']"));
	    myinfo.click();
	    Thread.sleep(3000);
	    
	    WebElement edits = driver.findElement(By.xpath("//input[@value='Edit']"));
	    edits.click();
	    
	    //Add Names
	    WebElement FName = driver.findElement(By.xpath("//input[@id='personal_txtEmpFirstName']"));
	    FName.click();
	    FName.clear();
	    FName.sendKeys("Firaars1");
	    
	    WebElement LName = driver.findElement(By.xpath("//input[@id='personal_txtEmpLastName']"));
	    LName.click();
	    LName.clear();
	    LName.sendKeys("Lasaars2");
	    
		//Add Gender    
	    WebElement genderfemale = driver.findElement(By.xpath("//input[@id='personal_optGender_2']"));
	    genderfemale.click();
	    
	    WebElement nationality = driver.findElement(By.xpath("//select[@id='personal_cmbNation']"));
	    Select s = new Select(nationality);
	    s.selectByVisibleText("Indian");
	    
		//Add DOB	    
	    WebElement EmpDOB = driver.findElement(By.xpath("//input[@id='personal_DOB']"));
	    EmpDOB.click();
	    EmpDOB.clear();
	    EmpDOB.sendKeys("1990-12-11");
	    
	    
	    WebElement Save = driver.findElement(By.xpath("//input[@id='btnSave']"));
	    Save.click();
	    		
	   	    
	    System.out.println("Employee details added");
		driver.close();	
		
		
		
		
	}


}


