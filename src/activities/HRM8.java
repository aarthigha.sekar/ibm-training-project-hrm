package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;


public class HRM8 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();
		
		Thread.sleep(3000);
		
		//Dashboard 
		WebElement Dashboard = driver.findElement(By.xpath("//a[@id='menu_dashboard_index']"));
		Dashboard.click();
		
		//Apply Leave
		WebElement Applyleave = driver.findElement(By.xpath("//span[text()='Apply Leave']"));
		Applyleave.click();
		
		WebElement leavetype = driver.findElement(By.xpath("//select[@id='applyleave_txtLeaveType']"));
	    Select leave = new Select(leavetype);
	    leave.selectByVisibleText("Paid Leave");
		
			    
		 WebElement fromdate = driver.findElement(By.xpath("//input[@id='applyleave_txtFromDate']"));
		    fromdate.click();
		    fromdate.clear();
		    fromdate.sendKeys("2020-02-20");
	        fromdate.submit();
		
        WebElement todate = driver.findElement(By.xpath("//input[@id='applyleave_txtToDate']"));
		    todate.click();
		    todate.clear();
		    todate.sendKeys("2020-02-21");		  
		    todate.submit();
		   
		  
		    
		WebElement Apply = driver.findElement(By.xpath("//input[@id='applyBtn']"));
			Apply.click();
				
			System.out.println("Leaves added");
	    //should add the verification of status of the leave application
		driver.close();
		
	   
		
	
	}


}


