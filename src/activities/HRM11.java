package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;

import java.awt.Desktop.Action;
import java.util.List;

import org.openqa.selenium.By;


public class HRM11 {

	public static void main(String[] args) throws InterruptedException {
		String name= "aaractivity3";
		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();
		
		Thread.sleep(3000);
		
		//Recruitment 
		WebElement recuritment = driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']"));
		recuritment.click();
		
		//Candidates
		WebElement candidates = driver.findElement(By.xpath("//a[@id='menu_recruitment_viewCandidates']"));
		candidates.click();
		
		WebElement canadd = driver.findElement(By.xpath("//input[@id='btnAdd']"));
	    canadd.click();
		
		WebElement Fname = driver.findElement(By.xpath("//input[@id='addCandidate_firstName']"));
		Fname.sendKeys(name);;
		 
		WebElement LName = driver.findElement(By.xpath("//input[@id='addCandidate_lastName']"));
	    LName.sendKeys("aarhrm");
	    
	    WebElement HManager = driver.findElement(By.xpath("//input[@id='addCandidate_email']"));	   
	    HManager.sendKeys("HRM11aar@alchemy.com");
	    	    
	    WebElement Vacancy = driver.findElement(By.xpath("//Select[@id='addCandidate_vacancy']"));
	   	Select vacy = new Select(Vacancy);	    
	    vacy.selectByVisibleText("Automation Specialist");  
	    
	    WebElement addresume=driver.findElement(By.xpath("//input[@id='addCandidate_resume']"));
	    addresume.sendKeys("C:\\Users\\AarthighaThoppe\\Desktop\\Fav.doc");
	   	    
	    WebElement save = driver.findElement(By.xpath("//input[@id='btnSave']"));
		save.click();

		System.out.println("portfolio saved");	
		Thread.sleep(4000);
		WebElement back = driver.findElement(By.xpath("//input[@id='btnBack']"));
		
		Actions a = new Actions(driver);
		a.moveToElement(back).click().build().perform();
		
		WebElement value = driver.findElement(By.xpath("(//a[contains(text(),'"+name+"')])[1]"));
		boolean displayed = value.isDisplayed();
		if(displayed==true) {
		System.out.println("portfolio verified");
		}
		else
		{
			System.out.println("portfolio not added");
		}
	    
		
	   		 
		driver.close();    
		    

}
}

