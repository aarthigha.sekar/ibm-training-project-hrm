package activities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


public class HRM10 {

	public static void main(String[] args) throws InterruptedException {
		String nm = "HRM ALCHEMY ACTIVITY4";
		System.setProperty("webdriver.chrome.driver","C:\\Users\\AarthighaThoppe\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
		//Login into webpage
		WebElement Username = driver.findElement(By.xpath("//input[@name='txtUsername']"));
		Username.sendKeys("orange");

		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("orangepassword123");

		WebElement Login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		Login.click();
		
		Thread.sleep(3000);
		
		//Recruitment 
		WebElement recuritment = driver.findElement(By.xpath("//a[@id='menu_recruitment_viewRecruitmentModule']"));
		recuritment.click();
		
		//Vacancy
		WebElement Vacancy = driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']"));
		Vacancy.click();
		Thread.sleep(3000);
		WebElement vacadd = driver.findElement(By.xpath("//input[@id='btnAdd']"));
		vacadd.click();
		Thread.sleep(3000);
		WebElement opentitle = driver.findElement(By.xpath("//Select[@id='addJobVacancy_jobTitle']"));
		Select s = new Select(opentitle);
		s.selectByVisibleText("DevOps Engineer");
		 
		WebElement VName = driver.findElement(By.xpath("//input[@id='addJobVacancy_name']"));
	    VName.sendKeys(nm);
	    
	    WebElement HManager = driver.findElement(By.xpath("//input[@id='addJobVacancy_hiringManager']"));
	    HManager.clear();
	    HManager.sendKeys("Firaar");
	    HManager.sendKeys(Keys.ENTER);
		  	    
	    
	    WebElement NOP = driver.findElement(By.xpath("//input[@id='addJobVacancy_noOfPositions']"));
	    NOP.sendKeys("25");
	    
	    WebElement save = driver.findElement(By.xpath("//input[@id='btnSave']"));
		save.click();
		
		WebElement back = driver.findElement(By.xpath("//input[@id='btnBack']"));
		back.click();
						
		WebElement value = driver.findElement(By.xpath("//a[text()='"+nm+"']"));
		boolean displayed = value.isDisplayed();
		if(displayed==true) {
		System.out.println("Vacancy added");
		}
		else
		{
			System.out.println("not added");
		}
	    
	    		 
		driver.close();    
		    

}
}

